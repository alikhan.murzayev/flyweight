package main

import (
	"bufio"
	"flyweight/flyweight"
	"fmt"
	"math/rand"
	"os"
	"strconv"
)

func main() {
	var factory flyweight.CarFactory
	var numCars = 1000

	volkswagenState := flyweight.State{
		Model:        "volkswagen",
		MaxSpeed:     240,
		EngineVolume: 3.6,
	}
	bmwState := flyweight.State{
		Model:        "bmw",
		MaxSpeed:     200,
		EngineVolume: 2.5,
	}

	// Open file
	f, err := os.Create("result.txt")
	if err != nil {
		panic(err.Error())
	}

	output := bufio.NewWriter(f)

	for num := 1; num <= numCars; num++ {

		var state flyweight.State
		number := strconv.Itoa(num)

		switch rand.Intn(2) {
		case 0:
			state = volkswagenState
		case 1:
			state = bmwState
		}

		car := factory.GetCar(state)
		car.SetNumber(number)

		_, _ = output.WriteString(car.String() + "\n")
	}
	_ = f.Close()

	fmt.Printf("States saved:\t%d\n", factory.GetNumStates())
	fmt.Printf("Cars saved:\t%d\n", numCars)
}
