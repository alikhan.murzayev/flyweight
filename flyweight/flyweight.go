// package flyweight implements cars storage using flyweight design pattern
package flyweight

import "fmt"

type Car interface {
	GetNumber() string
	SetNumber(number string)
	String() string
}

// Struct that represents a shared state over cars
type State struct {
	Model        string
	MaxSpeed     float64
	EngineVolume float64
}

func (s *State) String() string {
	return fmt.Sprintf("model - %s, max speed - %f, engine volume - %f.", s.Model, s.MaxSpeed, s.EngineVolume)
}

type CarFactory struct {
	pool map[State]Car
}

func (cf *CarFactory) GetCar(state State) Car {
	if cf.pool == nil {
		cf.pool = make(map[State]Car)
	}
	if _, ok := cf.pool[state]; !ok {
		cf.pool[state] = &ConcreteCar{state: state}
	}
	return cf.pool[state]
}

func (cf *CarFactory) GetNumStates() int {
	return len(cf.pool)
}

type ConcreteCar struct {
	state  State
	Number string
}

func (c *ConcreteCar) GetNumber() string {
	return c.Number
}

func (c *ConcreteCar) SetNumber(number string) {
	c.Number = number
}

func (c *ConcreteCar) String() string {
	return fmt.Sprintf("Number: %s. State: %v.", c.GetNumber(), c.state.String())
}
